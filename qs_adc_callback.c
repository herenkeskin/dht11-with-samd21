#include "asf.h"

struct port_config pin_conf;
struct usart_module usart_instance;

void serial_com_init(void);

void serial_com_init(void){
	
	struct usart_config config_usart;
	usart_get_config_defaults(&config_usart);
	config_usart.baudrate    = 115200;	// BAUDRATE ayarını cihazınıza göre düzenleyebilirsiniz.
	config_usart.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	config_usart.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	config_usart.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	config_usart.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	config_usart.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;
	
	stdio_serial_init(&usart_instance, EDBG_CDC_MODULE, &config_usart);
	usart_enable(&usart_instance);
	
}

#define DHTPIN		EXT2_PIN_3

void startSignal(void);
void checkSignal(void);

unsigned short k;

void DHT11StartSignal(){
	pin_conf.direction  = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(DHTPIN, &pin_conf);
	
	port_pin_set_output_level(DHTPIN, 0);
	delay_ms(18);
	
	port_pin_set_output_level(DHTPIN, 1);
	delay_us(30);
	
	pin_conf.direction  = PORT_PIN_DIR_INPUT;
	pin_conf.input_pull = PORT_PIN_PULL_UP;
	port_pin_set_config(DHTPIN, &pin_conf);
}

unsigned short DHT11CheckResponse(){
	
	k = 150;
	while(!port_pin_get_input_level(DHTPIN)){
		delay_us(2);
		k--;
		
		if(k < 1) {
			return 0; 
		}
	}
	k = 150;
	while(port_pin_get_input_level(DHTPIN)){
		delay_us(2);
		k--;
		
		if(k < 1) {
			return 0; 
		}
	}
	return 1;
}

unsigned short DHT11ReadByte(){

	int i;
	unsigned short num = 0;
	
	pin_conf.direction  = PORT_PIN_DIR_INPUT;
	pin_conf.input_pull = PORT_PIN_PULL_DOWN;
	port_pin_set_config(DHTPIN, &pin_conf);
	
	for (i = 0; i < 8; i++){
		while(!port_pin_get_input_level(DHTPIN));
		delay_us(40);
		
		if(port_pin_get_input_level(DHTPIN)) {
			num |= 1 << (7 - i);
		}
	}
	return num;
}

int main(void){
	
	system_init();
	serial_com_init();
	delay_init();
	printf( "SAMD21 - DHT11 Temperature test program\n" );
	
	unsigned short data[5];
	uint8_t i;
	
	while(1){
		
		DHT11StartSignal();
		if(!DHT11CheckResponse()){
			 continue;
		}
		
		for(i = 0; i < 5; i++){
			data[i] = DHT11ReadByte();
			printf("data[%d] = %d\r\n", i, data[i]);
		}
		
		data[0] = (data[0] / 10) + 48;
		data[1] = (data[1] % 10) + 48;
		data[2] = (data[2] / 10) + 48;
		data[3] = (data[3] % 10) + 48;
		
		printf("Humidity: %d.%d %\r\n", data[0], data[1]);
		
		uint8_t checksum = data[0] + data[1] + data[2] + data[3];
		
		for(i = 0; i < 5; i++){
			printf("data[%d] = %d\r\n", i, data[i]);
		}
		
		if(data[4] == checksum){
			printf("Esitmis\r\n");
		}else {
			printf("Esit degilmis\r\n");
		}

		delay_ms(1000);
		
	}
	
	return 0;
}