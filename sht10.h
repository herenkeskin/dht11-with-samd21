///*
//* sht10.h
//*
//* Created: 27.01.2016 00:31:57
//*  Author: Edin
//*/
//
//#ifndef SHT10_H
//#define SHT10_H
//
///* SHT10 Sensor commands definitions */
//#define SHT_MEASURE_TEMPERATURE 0b00000011
//#define SHT_MEASURE_HUMIDITY 0b00000101
//#define SHT_READ_STATUS 0b00000111
//#define SHT_WRITE_STATUS 0b00000110
//#define SHT_RESET 0b00011110
//
///* Parameters for the SHT10 Sensor
//*  Structure that keeps the parameters for a SHT10 sensor
//*  Contains clk and data pin assignments (port,pin,mux identifier), 
//*  command parameter to be sent to sensor,
//*  rawData read from the sensor
//*/
//typedef struct{
	//uint32_t clk;
	//uint32_t data;
	//uint8_t command;
	//uint16_t rawData;
//}SHT10parameters ;
//
///// Switch between data pin configuration/definition
///// @param SHT10parameters * PAR Pointer to SHT10 sensor parameters structure
///// @param port_config * CFG Pointer to port configuration parameters structure
///// @param mode parameter responsible for changing data pin configuration, 1 for input, 0 for output
//
//static void pinMode_SHT10(SHT10parameters * PAR, struct port_config * CFG, bool mode);
//
///// Configure SHT10 sensor
///// Data and clock pins are configured in this function
///// @param SHT10parameters * PAR Pointer to SHT10 sensor parameters structure
///// @param port_config * CFG Pointer to port configuration parameters structure
//
//void configure_SHT10(SHT10parameters * PAR, struct port_config * CFG);
//
///// Reset connection and internal parameters of SHT10 sensor.
///// Sensors is reset to default.
///// @param SHT10parameters * PAR Pointer to SHT10 sensor parameters structure.
///// @param port_config * CFG Pointer to port configuration parameters structure.
//
//void resetSHT10(SHT10parameters * PAR, struct port_config * CFG);
//
///// Reset connection and internal parameters of SHT10 sensor.
///// Sensors is reset to default.
///// @param SHT10parameters * PAR Pointer to SHT10 sensor parameters structure.
///// @param port_config * CFG Pointer to port configuration parameters structure
//
//uint8_t sendCommandSHT10(SHT10parameters * PAR, struct port_config * CFG);
//
//uint8_t readDataSHT10(SHT10parameters * PAR, struct port_config * CFG);
//
//void readRawDataSHT10(SHT10parameters * PAR, struct port_config * CFG);
//
//static uint8_t waitForResultSHT10(SHT10parameters * PAR, struct port_config * CFG);
//
//float readTemperatureSHT10(SHT10parameters * PAR, struct port_config * CFG);
//
//float readHumiditySHT10(SHT10parameters * PAR, struct port_config * CFG);
//
//#endif