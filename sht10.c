///*
//* sht10.c
//*
//* Created: 27.01.2016 00:31:57
//*  Author: Edin
//*/
//
//#include <asf.h>
//#include <sht10.h>
//
//// Configure SHT
//// data_pin: port pin for data interface
//// data_clk:  port pin for clk interface
//// SHTparameters: struct with SHT sensor parameters
//
//void configure_SHT10(SHT10parameters * PAR, struct port_config * CFG)
//{
	//// Configure data pin
	//CFG->direction  = PORT_PIN_DIR_OUTPUT;
	//CFG->input_pull = PORT_PIN_PULL_NONE;
	//port_pin_set_config(PAR->data, CFG);
	//
	//// Configure clk pin
	//CFG->direction  = PORT_PIN_DIR_OUTPUT;
	//CFG->input_pull = PORT_PIN_PULL_NONE;
	//port_pin_set_config(PAR->clk, CFG);	
//}
//
//// Change mode of pin
//// 0 for output
//// 1 for input
//
//static void pinMode_SHT10(SHT10parameters * PAR, struct port_config * CFG, bool mode){
	//CFG->input_pull = PORT_PIN_PULL_NONE;
	//if (mode!=0)
	//{
		//CFG->direction  = PORT_PIN_DIR_INPUT;
	//}
	//else
	//{
		//CFG->direction  = PORT_PIN_DIR_OUTPUT;
	//}
	//port_pin_set_config(PAR->data, CFG);
//}
//
//// Wait for results function
//
//static uint8_t waitForResultSHT10(SHT10parameters * PAR, struct port_config * CFG){
//
	//uint8_t i;
	//bool ack;
	//uint8_t success;
	//
	//// wait for 100 milliseconds until the sensor finish with conversion
	//// this waiting loop can be implemented inside the application
	//// once the data is converted (~80 milliseconds), it can be read any time
	//pinMode_SHT10(PAR, CFG, 1);
	//for(i= 0; i < 100; ++i)
	//{
		//delay_ms(3);
		//ack = port_pin_get_input_level(PAR->data);
		//
		//if (!ack) {
			//success = 1;
			//break;
		//}
	//}
//
	//if (ack){
		//success=0;
	//}
	//return success;
//}
//
//// Send command to sensor
//
//uint8_t sendCommandSHT10(SHT10parameters * PAR, struct port_config * CFG)
//{
	//volatile bool resultBit = 0;
	//volatile uint8_t countBit = 0;
	//volatile bool ack = 0;
	//volatile uint8_t success = 0;
	//
	//
	//// Transmission Start Sequence
	///* To initiate a transmission, a Transmission Start sequence has to be issued. It consists of a lowering of the DATA line
	    //while SCK is high, followed by a low pulse on SCK and raising DATA again while SCK is still high */
	//pinMode_SHT10(PAR, CFG, 0);
	//
	//port_pin_set_output_level(PAR->data, true); delay_us(10);
	//port_pin_set_output_level(PAR->clk, true); delay_us(10);
	//port_pin_set_output_level(PAR->data, false); delay_us(10);
	//port_pin_set_output_level(PAR->clk, false); delay_us(10);
	//port_pin_set_output_level(PAR->clk, true); delay_us(10);
	//port_pin_set_output_level(PAR->data, true); delay_us(10);
	//port_pin_set_output_level(PAR->clk, false); delay_us(10);
	//port_pin_set_output_level(PAR->data, false); delay_us(10);
	//
	//// Send command to SHT10
//
	//PAR->command=PAR->command & 0b00011111;  // perform "and" on command with mask to make sure first 3 bits of command are zero 
    //
	//for(countBit = 8; countBit > 0; countBit--)
	//{
		//resultBit = (PAR->command >> (countBit-1)) & 0b00000001;
		//port_pin_set_output_level(PAR->data, resultBit); delay_us(10);
		//port_pin_set_output_level(PAR->clk, true); delay_us(10);
		//port_pin_set_output_level(PAR->clk, false); delay_us(10);
	//}
	//
	//// Get acknowlodge signal
	//port_pin_set_output_level(PAR->data, true); delay_us(10);
	//port_pin_set_output_level(PAR->clk, true); delay_us(10);
	//
	//pinMode_SHT10(PAR, CFG, 1);
	//
	//ack=port_pin_get_input_level(PAR->data);delay_us(10);
	//port_pin_set_output_level(PAR->clk, false); delay_us(10);
	//
	//// return success or fail
	//
	//if (!ack){
		//success=1;
	//} 
	//else{
		//success=0;
	//}
	//
	//return success;
//}
//
//
//// read raw data and crc value
//uint8_t readDataSHT10(SHT10parameters * PAR, struct port_config * CFG){
//
	//volatile bool resultBit = 0;
	//volatile uint8_t countBit = 0;
//
	//volatile uint8_t rawDataMSB = 0;
	//volatile uint8_t rawDataLSB = 0;
	//volatile uint8_t rawDataCRC = 0;
	//volatile uint8_t dummy=0;
//
//
	///* Two bytes of measurement data and one byte of CRC checksum (optional) will be transmitted upon the completion
	   //of measurement. The microcontroller must acknowledge each byte by pulling the DATA line low. All values are 
	   //most significant bit first, right justified (e.g. the 5th SCK is most significant bit for a 12bit value, 
	   //for an 8bit result the first byte is not used). */
//
	//// read MSB
//
	////port_pin_set_output_level(PAR->data, false); delay_us(10);
	//
	//pinMode_SHT10(PAR, CFG, 1);
	//
	//for(countBit = 8; countBit > 0; countBit--)
	//{
		//port_pin_set_output_level(PAR->clk, true); delay_ms(2);
		//dummy=port_pin_get_input_level(PAR->data);
		//rawDataMSB= rawDataMSB | dummy<<(countBit-1);
		//port_pin_set_output_level(PAR->clk, false); delay_ms(2);
	//}
	//
	//pinMode_SHT10(PAR, CFG, 0);
	//
	//// Send acknowledge signal to sensor after MSB is read
	//port_pin_set_output_level(PAR->data, true); delay_ms(1);
	//port_pin_set_output_level(PAR->data, false); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, true); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, false); delay_ms(1);
//
	//// read LSB
	//
	//pinMode_SHT10(PAR, CFG, 1);
	//
	//for(countBit = 8; countBit > 0; countBit--)
	//{
		//port_pin_set_output_level(PAR->clk, true); delay_ms(2);
		//dummy=port_pin_get_input_level(PAR->data);
		//rawDataLSB= rawDataLSB | dummy<<(countBit-1);
		//port_pin_set_output_level(PAR->clk, false); delay_ms(2);
	//}
	//
	//pinMode_SHT10(PAR, CFG, 0);
	//
	//// Send acknowledge signal to sensor after LSB is read
	//
	//port_pin_set_output_level(PAR->data, true); delay_ms(1);
	//port_pin_set_output_level(PAR->data, false); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, true); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, false); delay_ms(1);
//
	//// read CRC
	//
	//pinMode_SHT10(PAR, CFG, 1);
	//
	//for(countBit = 8; countBit > 0; countBit--)
	//{
		//port_pin_set_output_level(PAR->clk, true); delay_ms(2);
		//dummy=port_pin_get_input_level(PAR->data);
		//rawDataCRC= rawDataCRC | dummy<<(countBit-1);
		//port_pin_set_output_level(PAR->clk, false); delay_ms(2);
	//}
	//
	//// terminate the connection and put sensor to sleep
//
	//pinMode_SHT10(PAR, CFG, 0);
	//
	//port_pin_set_output_level(PAR->data, true); delay_ms(1);
	//port_pin_set_output_level(PAR->data, false); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, true); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, false); delay_ms(1);
	//
	//// one can additionally check CRC here...
//
	//// unify MSB and LSB and store value in the struct.rawData
	//
	//PAR->rawData= (rawDataMSB<<8) | rawDataLSB;
//
	//return 1;
//
//}
//
//void resetSHT10(SHT10parameters * PAR, struct port_config * CFG){
//
	//volatile uint8_t i=0;
	//// Perform connection reset
	//// While leaving DATA high, toggle SCK nine or more times
	//pinMode_SHT10(PAR, CFG, 0);
	//port_pin_set_output_level(PAR->data, true); delay_ms(1);
	//port_pin_set_output_level(PAR->clk, true); delay_ms(1);
	//
	//for(i = 0; i <=15 ; i++) {
		//port_pin_toggle_output_level(PAR->clk);
		//delay_ms(1); 
	//}
//
	//delay_ms(1);
//
	//// Perform soft reset by sending 11110 command to sensor
    //// Soft reset, resets the interface, clears the status register to default values. 
    //// Wait minimum 11 ms before next command
	//
	//PAR->command=SHT_RESET;
	//sendCommandSHT10(PAR, CFG);
//
	//delay_ms(150);
//}
//
//float readTemperatureSHT10(SHT10parameters * PAR, struct port_config * CFG){
//
	//// Conversion coefficients from SHT10 datasheet
  	//const float D1 = -39.65;  // for 14 Bit @ 3.3V
  	//const float D2 =   0.01;  // for 14 Bit DEGC
//
	//PAR->command = SHT_MEASURE_TEMPERATURE;
	//sendCommandSHT10(PAR, CFG);
	//waitForResultSHT10(PAR, CFG);
	//readDataSHT10(PAR, CFG);
//
	//return ((PAR->rawData & 0x3FFF)*D2) + D1;
//}
//
//void readRawDataSHT10(SHT10parameters *PAR,struct port_config *CFG)
//{
	//sendCommandSHT10(PAR, CFG);
	//waitForResultSHT10(PAR, CFG);
	//readDataSHT10(PAR, CFG);
//}
//
//float readHumiditySHT10(SHT10parameters * PAR, struct port_config * CFG){
//
	//// Conversion coefficients from SHT15 datasheet
  	//const float C1 = -2.0468;       // for 12 Bit
  	//const float C2 =  0.0367;       // for 12 Bit
  	//const float C3 = -0.0000015955; // for 12 Bit
  	//const float T1 =  0.01;         // for 14 Bit
  	//const float T2 =  0.00008;      // for 14 Bit
  	//float linearHumidity = 0;
  	//float temperature = 0;
  	//float rawHumidity = 0;
//
	//PAR->command = SHT_MEASURE_HUMIDITY;
	//sendCommandSHT10(PAR, CFG);
	//waitForResultSHT10(PAR, CFG);
	//readDataSHT10(PAR, CFG);
//
//
	//rawHumidity = PAR->rawData & 0x0FFF; 
	//linearHumidity = C1 + C2 * (rawHumidity) + C3 * (rawHumidity) * (rawHumidity);
	//
	//temperature=readTemperatureSHT10(PAR, CFG);
//
	//return (temperature - 25) * (T1 + T2*rawHumidity) + linearHumidity;
//}
//
